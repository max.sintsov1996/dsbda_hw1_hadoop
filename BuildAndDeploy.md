Build and deploy manual
=======================

## Required tools

- jdk (version 7 or later)
- maven
- docker
- git

## Preparation

Create working directory

```
mkdir hadoop-longest-word
cd hadoop-longest-word
```

Download sources

```
git clone https://gitlab.com/max.sintsov1996/dsbda_hw1_hadoop.git
git clone https://github.com/kiwenlau/hadoop-cluster-docker
```

Download hadoop docker image and create hadoop network

```
docker pull kiwenlau/hadoop:1.0
docker network create --driver=bridge hadoop
```

## Build

Build application with maven

```
cd hw1-hadoop
mvn package
cd ..
```


## Deploy

### Prepare containers

Start docker containers

```
cd hadoop-cluster-docker
./start-container.sh
cd ..
```
You shold receive the following output and get into master's shell

```
start hadoop-master container...
start hadoop-slave1 container...
start hadoop-slave2 container...
```

Start hadoop inside master container

```
./start-hadoop.sh
```

Test on word count

```
./run-wordcount.sh
```

You shold receive the following output

```
input file1.txt:
Hello Hadoop

input file2.txt:
Hello Docker

wordcount output:
Docker    1
Hadoop    1
Hello    2
```

### Deploy jar file

Open a new terminal with host shell and cd into "hadoop-longest-word" directory

Find out your master container id

```
MASTERID=$(docker ps | grep hadoop-master | cut -d " " -f 1)
```

Copy jar file to docker container

```
docker cp dsbda_hw1_hadoop/target/homework1-1.0-SNAPSHOT-jar-with-dependencies.jar $MASTERID:/root
```

### Test / Run

Clone full dataset

https://drive.google.com/file/d/1PY_7TF19FCwaFbuZv6K31l71NXmALYvH/view?usp=sharing

or use the example date set located inputs

Alternatively : use your own input data

Copy test file to master container

```
docker cp -r input $MASTERID:/root
```

Switch to hadoop master container's shell

Put test data to hdfs

```
hdfs dfs -mkdir lw-input
hdfs dfs -put input lw-input

```

Run longest word job

```
hadoop jar homework1-1.0-SNAPSHOT-jar-with-dependencies.jar lw-input lw-output
```

Check the result

```
hdfs dfs -cat lw-output/part-r-00000
```