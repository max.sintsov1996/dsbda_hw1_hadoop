package bd.homework1;

/**
 * Тип счётчика для подсчета статистики по битым логам
 */
public enum CounterType {
    MALFORMED,
    FILE_NOT_FOUND,
    SOME_OTHER_ERROR
}
