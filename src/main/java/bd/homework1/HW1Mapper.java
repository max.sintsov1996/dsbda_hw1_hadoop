package bd.homework1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

/**
 * Mapper compare city to name, gives out city of line with high-bid-priced
 */
public class HW1Mapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private final static IntWritable one = new IntWritable(1);
    private static final String delimiters = "\t";
    private Text word = new Text();

    private static HashMap<String, String> CityMap = new HashMap<String, String>();
    private BufferedReader brReader;


    private final int BiddingPriseColumn = 20 - 1;
    private final int CityColumn = 8 - 1;

    @Override
    protected void setup(Context context) throws IOException{

        URI[] cacheFilesLocal = context.getCacheFiles();

        loadCityHashMap(cacheFilesLocal[0], context);

    }


    private void loadCityHashMap(URI filePath, Context context)
            throws IOException {

        String strLineRead = "";

        try {
            brReader = new BufferedReader(new FileReader(filePath.toString()));

            // Read each line, split and load to HashMap
            while ((strLineRead = brReader.readLine()) != null) {
                String[] deptFieldArray = strLineRead.split(delimiters);
                CityMap.put(deptFieldArray[0].trim(),
                        deptFieldArray[1].trim());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            context.getCounter(CounterType.FILE_NOT_FOUND).increment(1);
        } catch (IOException e) {
            context.getCounter(CounterType.SOME_OTHER_ERROR).increment(1);
            e.printStackTrace();
        }finally {
            if (brReader != null) {
                brReader.close();
            }
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] line = value.toString().split("\t");
        String city;
        if(line.length == 24) {
            if (Integer.parseInt(line[BiddingPriseColumn]) > 250) {
                city = CityMap.get(line[CityColumn]);
                if(city == null){
                    context.getCounter(CounterType.MALFORMED).increment(1);
                }
                else{
                    word.set(city);
                    context.write(word, one);
                }
            }
        }
    }
}
