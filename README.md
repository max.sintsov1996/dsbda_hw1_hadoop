Data science lab1 - hadoop
=====
## Task

### Business logic

Program which calculate amount of high-bid-priced (more than 250 on 20 column of imp* files with
structure of each row impression events by city, where each city presented with its name rather than id.

### Output format

SequenceFile

### Additional

Counters is used for statistics about malformed rows collection

### Report includes

- Screenshot of successfully uploaded file into HDFS
- Screenshots of successfully executed job and result
- Screenshot with information about memory consumption

## Links

[Full data set](https://drive.google.com/file/d/1PY_7TF19FCwaFbuZv6K31l71NXmALYvH/view?usp=sharing)

